package ServerDefault

import (
	"../../../../System/Application/Model"
	"../../../../System/Application/Run"
	"fmt"
	"time"
)

type Site interface {
	Index()
	Test()
}

type ServerSite struct {
}

func (serverSite ServerSite) Index(params Run.Params) {
	fmt.Println("调用控制器方法")
	fmt.Println(params)
	//
	serverSite.Test2(params)
}

func (ServerSite ServerSite) Test2(params Run.Params) {
	ticker := time.NewTicker(time.Millisecond * 500)
	go func() {
		for t := range ticker.C {
			fmt.Println("Tick at", t)
		}
	}()
	time.Sleep(time.Millisecond * 3100)
	ticker.Stop()
	fmt.Println("123")
}

func (ServerSite ServerSite) Test1(params Run.Params) {

	// For our example we'll select across two channels.
	c1 := make(chan string)
	c2 := make(chan string)

	// Each channel will receive a value after some amount
	// of time, to simulate e.g. blocking RPC operations
	// executing in concurrent goroutines.
	go func() {
		time.Sleep(time.Second * 3)
		c1 <- "three"
	}()
	go func() {
		time.Sleep(time.Second * 2)
		c2 <- "two"
	}()

	// We'll use `select` to await both of these values
	// simultaneously, printing each one as it arrives.
	for {
		select {
		case msg1 := <-c1:
			fmt.Println("received", msg1)
		case msg2 := <-c2:
			fmt.Println("received", msg2)
		case msg2 := <-c2:
			fmt.Println("received", msg2)
		case <-time.After(time.Second * 1):
			fmt.Println("received timeout")
			//default:
			//	fmt.Println("received stop")
		}
		//time.Sleep(time.Second * 1)
	}
}

func (serverSite ServerSite) Test(params Run.Params) {
	//fmt.Println("调用控制器方法")
	//fmt.Println(params)
	//
	var mysqlDb Model.DbInterface
	mysqlDb = new(Model.MysqlDb)
	_, openErr := mysqlDb.Open("127.0.0.1", 3306, "test", "golang", "12345678")
	if openErr != nil {
		fmt.Println(openErr)
	}
	result, executeErr := mysqlDb.Execute("insert into `test`(`code`,`parent_code`,`name`,`content`,`created`,`updated`,`status`) values(?,?,?,?,?,?,?);", "test_2", 0)
	if executeErr != nil {
		fmt.Println(executeErr)
	}
	if result != nil {
		fmt.Println(result.LastInsertId())
	}

	maps, queryErr := mysqlDb.Query("select `id`,`name`,`status` from `test` where `id`>? order by `id` asc;", 0)
	if queryErr != nil {
		fmt.Println(queryErr)
	}
	//查询出的数据
	fmt.Println(maps)

	closeErr := mysqlDb.Close()
	if closeErr != nil {
		fmt.Println(closeErr)
	}
	//ok, clearErr := mysqlDb.Clear()
	//fmt.Println(ok)
	//fmt.Println(clearErr)
}
