package Model

import (
	"errors"
)

type DbAccessInterface interface {
	Connection(key string) (DbInterface, error) //打开一个连接
	Find() (map[uint64]map[string]interface{}, error)
	FindOne() (map[string]interface{}, error)
	Insert() (uint64, error)
	Update() (uint64, error)
	Delete() (uint64, error)
}

type MysqlDbAccess struct {
	key           string
	is_connection bool
	msg           error
	db_name       string
	tb_name       string
	tb_fields     *map[string]interface{}
}

func (mysqlDbAccess *MysqlDbAccess) Connection(key string) (DbInterface, error) {
	if mysqlDbAccess == nil {
		return nil, errors.New("进行数据库操作时，发现数据库操作对象句柄是空的")
	}
	//连接数据库
	return nil, nil
}

func (mysqlDbAccess *MysqlDbAccess) Find() (map[uint64]map[string]interface{}, error) {
	if mysqlDbAccess == nil {
		return nil, errors.New("进行数据库操作时，发现数据库操作对象句柄是空的")
	}
	if !mysqlDbAccess.is_connection {
		return nil, errors.New("进行数据库操作时，发现数据库未连接")
	}
	//查询数据（多行）@todo
	return nil, nil
}

func (mysqlDbAccess *MysqlDbAccess) Insert() (uint64, error) {
	if mysqlDbAccess == nil {
		return 0, errors.New("进行数据库操作时，发现数据库操作对象句柄是空的")
	}
	if !mysqlDbAccess.is_connection {
		return 0, errors.New("进行数据库操作时，发现数据库未连接")
	}
	//新增数据（一行）@todo
	return 0, nil
}

func (mysqlDbAccess *MysqlDbAccess) Update() (uint64, error) {
	if mysqlDbAccess == nil {
		return 0, errors.New("进行数据库操作时，发现数据库操作对象句柄是空的")
	}
	if !mysqlDbAccess.is_connection {
		return 0, errors.New("进行数据库操作时，发现数据库未连接")
	}
	//更新数据（一行）@todo
	return 0, nil
}

func (mysqlDbAccess *MysqlDbAccess) Delete() (uint64, error) {
	if mysqlDbAccess == nil {
		return 0, errors.New("进行数据库操作时，发现数据库操作对象句柄是空的")
	}
	if !mysqlDbAccess.is_connection {
		return 0, errors.New("进行数据库操作时，发现数据库未连接")
	}
	//删除数据（一行）@todo
	return 0, nil
}
