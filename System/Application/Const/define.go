package Const

const RUN_MODE_CLI = "cli"
const RUN_MODE_WEB = "web"
const RUN_MODE_SOCKET_TCP = "socket_tcp"
const RUN_MODE_SOCKET_UDP = "socket_udp"
const RUN_MODE = RUN_MODE_WEB
const DEBUG bool = true
const SERVER_IP string = "127.0.0.1"
const SERVER_PORT int = 10260
