package Run

import (
	"fmt"
	"net/http"
	"strconv"
)

type WebInterface interface {
	Handler()
	Listen()
	Send()
	Receive()
}

type Web struct {
	request  *http.Request
	response *http.Response
}

func WebMain() {

}

func (web *Web) Listen(port int) {
	http.ListenAndServe(":"+strconv.Itoa(port), nil)
}

//输入输出
func HandlerFunc(request *http.Request, writer http.ResponseWriter) {

}

func (web *Web) GetWriter() http.ResponseWriter {

}

func (web *Web) Send() {

}
