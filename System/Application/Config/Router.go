package Config

import (
	"../../../Client/Application/Controller/ClientDefault"
	"../../../Server/Application/Controller/ServerDefault"
	"reflect"
)

type RouterInterface interface {
	Table() map[string]map[string]reflect.Type
}

type Router struct {
}

func (router Router) Table() map[string]map[string]reflect.Type {
	//
	Table := make(map[string]map[string]reflect.Type)
	//
	var serverSite ServerDefault.ServerSite
	var clientSite ClientDefault.ClientSite
	//
	Table["server/default/site/index"] = make(map[string]reflect.Type)
	Table["server/default/site/index"]["Server/Application/Controller/ServerDefault/Site/Index"] = reflect.TypeOf(serverSite)
	//
	Table["server/default/site/test"] = make(map[string]reflect.Type)
	Table["server/default/site/test"]["Server/Application/Controller/ServerDefault/Site/Test"] = reflect.TypeOf(serverSite)
	//
	Table["client/default/site/index"] = make(map[string]reflect.Type)
	Table["client/default/site/index"]["Client/Application/Controller/ClientDefault/Site/Index"] = reflect.TypeOf(clientSite)
	//
	Table["client/default/site/test"] = make(map[string]reflect.Type)
	Table["client/default/site/test"]["Server/Application/Controller/ClientDefault/Site/Test"] = reflect.TypeOf(clientSite)
	//
	//fmt.Println(Table)
	//
	return Table
}
