package Lib

//连接状态
const SERVER_SOCKET_LISTEN = 1 //连接已创建

const SERVER_SOCKET_ACCEPT = 2 //连接已可以接受客户端

const SERVER_SOCKET_CREATE = 3 //连接已创建

const SERVER_SOCKET_EXEC = 4 //连接已创建

const SERVER_SOCKET_WAIT_CLOSE = 0 //连接已发出关闭申请

const SERVER_SOCKET_CLOSE = -1 //连接已创建

const SERVER_SOCKET_CLEAR = -2 //连接已创建

const CLIENT_SOCKET_CREATE = 1 //连接已启动

const CLIENT_SOCKET_EXEC = 2 //连接已开始传输

const CLIENT_SOCKET_WAIT_CLOSE = 0 //连接已发出关闭申请

const CLIENT_SOCKET_CLOSE = -1 //连接已关闭

const CLIENT_SOCKET_CLEAR = -2 //连接已释放

//创建监听通用回调函数
type ServerSocketCallback func(map[string]interface{}) interface{}

//创建连接通用回调函数
type SocketCallback func(map[string]interface{}) interface{}

//连接池接口
type ServerSocketPoolInterface interface {
	CreateServerSocket() //创建指定监听
	GetServerSocket()    //获得指定连接
}

//连接池接口
type SocketPoolInterface interface {
	CreateSocket()     //创建指定连接
	GetSocket()        //获得指定连接
	GetCurrentSocket() //获得当前连接
	GetParentSocket()  //获得指定连接的父连接
	GetChildSockets()  //获得指定连接的子连接组
	ClearSocket()      //释放指定连接
	StartSocket()      //启动指定连接
	waitSocket()       //暂停指定连接
	StopSocket()       //终止指定连接
	ReStartSocket()    //重启指定连接（重新开始）
}

//监听接口
type ServerSocketInterface interface {
}

//连接接口
type SocketInterface interface {
}

//监听池结构
type ServerSocketPool struct {
	ServerSocketMap map[string]ServerSocket //连接池
}

//连接结构
type ServerSocket struct {
	id           uint64
	name         string
	status       int
	startTime    int
	callbackName string
	callback     ServerSocketCallback
}

//连接池结构
type SocketPool struct {
	SocketMap     map[string]Socket           //连接池
	inputPipeMap  map[string]chan interface{} //连接池的输入管道池（通常，每个连接都会持有输入管道池中的一条管道）
	outputPipeMap map[string]chan interface{} //连接池的输出管道池（通常，每个连接都会持有输出管道池中的一条管道）
	count         uint64                      //连接池大小（已经存在的连接数量）
}

//连接结构
type Socket struct {
	id           uint64
	name         string
	status       int
	startTime    int
	callbackName string
	callback     SocketCallback
}
