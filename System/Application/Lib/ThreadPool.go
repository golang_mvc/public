package Lib

import "time"

//线程状态
const THREAD_CREATE = 1 //线程已创建

const THREAD_START = 2 //线程已启动

const THREAD_STOP = 0 //线程已终止

const THREAD_WAIT = 3 //线程被暂停

const THREAD_CLOSE = -1 //线程已关闭

const THREAD_CLEAR = -2 //线程已释放

//创建通用回调函数(必传参数：map[[name string:=回调函数名称],[threadName * Thread:=调用回调函数的线程指针])
type ThreadCallback func(map[string]interface{}) interface{}

//线程池接口
type ThreadPoolInterface interface {
	CreateThread()               //创建指定线程
	GetThread()                  //获得指定线程
	GetThreadByName(name string) //
	GetThreadById(id uint64)     //
	GetCurrentThread()           //获得当前线程
	GetParentThread()            //获得指定线程的父线程
	GetChildThreads()            //获得指定线程的子线程组
	ClearThread()                //释放指定线程
	StartThread()                //启动指定线程
	waitThread()                 //暂停指定线程
	StopThread()                 //终止指定线程
	ReStartThread()              //重启指定线程（重新开始）
}

//线程接口
type ThreadInterface interface {
	GetId() uint64           //获得线程ID（线程ID为创建线程时，由创建线程方法自动生成（64位无符号整数，整数长度：18））
	GetName() string         //获得线程名称（线程名称由程序员创建线程时手动指定，调用线程时，程序员需了解被调用线程的线程名称）
	GetStatus() int          //获得线程状态（创建，启动，暂停，终止，关闭，释放）
	GetCallbackName() string //获得线程的回调函数名称
	GetStartTime() int       //获得线程的开始执行时间（10位UNIX时间戳：精确到秒）
	GetExecTime() int        //获得线程的已执行时间（10位UNIX时间戳，精确到秒，当前时间-开始时间，用于统计和分析线程性能瓶颈）
	GetParentId() uint64     //获得父线程ID
	GetChildIds() []uint64   //获得子线程ID集合
	addChildId() uint64      //向子线程ID集合中追加新的子线程ID
}

//线程池结构
type ThreadPool struct {
	threadNameMap map[string]Thread //线程池
	threadIdMap   map[uint64]Thread //线程池
	count         uint64            //线程池大小（已经存在的线程数量）
}

//线程结构
type Thread struct {
	inputPipe    chan interface{} //输入管道
	outputPipe   chan interface{} //输出管道
	parentId     uint64           //当前线程的父线程ID
	id           uint64           //当前线程ID
	name         string           //当前线程名称
	status       int              //当前线程状态
	startTime    int32            //当前线程的启动时间
	childIds     []uint64         //当前线程的子线程ID集合
	callbackName string           //当前线程的回调函数名称
	callback     ThreadCallback   //当前线程的回调函数
}

//创建管道
func (thread *Thread) CreatePipe(chanNumber uint64) chan interface{} {
	var pipe chan interface{} = nil
	if chanNumber <= 0 {
		pipe = make(chan interface{})
	} else {
		pipe = make(chan interface{}, chanNumber)
	}
	return pipe
}

//清除管道
func (thread *Thread) ClearPipe(pipe *chan interface{}) {
	pipe = nil
}

//创建输入流管道
func (thread *Thread) CreateInputPipe(chanNumber uint64) chan interface{} {
	thread.inputPipe = thread.CreatePipe(chanNumber)
	return thread.inputPipe
}

//创建输出流
func (thread *Thread) CreateOutputPipe(chanNumber uint64) chan interface{} {
	thread.outputPipe = thread.CreatePipe(chanNumber)
	return thread.outputPipe
}

//获得输入流管道
func (thread *Thread) GetInputPipe() chan interface{} {
	return thread.inputPipe
}

//获得输出流管道
func (thread *Thread) GetOutputPipe() chan interface{} {
	return thread.outputPipe
}

//清除输出流管道
func (thread *Thread) ClearInputPipe() {
	thread.ClearPipe(&(thread.inputPipe))
}

//清除输入流管道
func (thread *Thread) ClearOutputPipe() {
	thread.ClearPipe(&(thread.outputPipe))
}

//创建线程
func (threadPool *ThreadPool) CreateThread(name string, inputPipe chan interface{}, outputPipe chan interface{}, callbackName string, callback ThreadCallback, parentId uint64) uint64 {
	//
	threadPool.count++ //线程池计数器自增
	//
	var thread Thread
	thread.inputPipe = inputPipe                //输入管道
	thread.outputPipe = outputPipe              //输出管道
	thread.parentId = parentId                  //当前线程的父线程ID
	thread.id = threadPool.count                //当前线程ID
	thread.name = name                          //当前线程名称
	thread.status = THREAD_CREATE               //当前线程状态
	thread.startTime = int32(time.Now().Unix()) //当前线程的启动时间
	thread.childIds = make([]uint64, 0)         //当前线程的子线程ID集合
	thread.callbackName = callbackName          //当前线程的回调函数名称
	thread.callback = callback                  //当前线程的回调函数
	if thread.parentId > 0 {                    //如果当前线程存在父线程，将当前线程的ID追加到父线程的子线程集合中
		threadPool.GetThreadById(thread.parentId).childIds = append(threadPool.GetThreadById(thread.parentId).childIds, thread.id)
	}
	threadPool.threadIdMap[thread.id] = thread
	threadPool.threadNameMap[thread.name] = thread
	return thread.id
}

//获得线程
func (threadPool *ThreadPool) GetThreadByName(name string) *Thread {
	return nil
}

//获得线程
func (threadPool *ThreadPool) GetThreadById(id uint64) *Thread {
	return nil
}

//获得当前线程
func (thread *Thread) GetCurrentThread() uint64 {
	return 0
}

//获得当前线程的父线程
func (thread *Thread) GetParentThread() uint64 {
	return 0
}

//获得当前线程的所有子线程
func (thread *Thread) GetChildThreads() []uint64 {
	return nil
}

//清除线程
func (threadPool *ThreadPool) ClearThread(name string) []uint64 {
	return nil
}

//启动线程
func (threadPool *ThreadPool) StartThread(name string) []uint64 {
	//map中包含当前线程的输入输出管道流
	//go (threadPool.GetThread(name)).callback(map)
	return nil
}

//暂停线程
func (threadPool *ThreadPool) waitThread(name string) []uint64 {
	return nil
}

//停止线程
func (threadPool *ThreadPool) StopThread(name string) []uint64 {
	return nil
}

//重启线程
func (threadPool *ThreadPool) ReStartThread(name string) []uint64 {
	return nil
}

//获得线程ID
func (thread *Thread) GetId() uint64 {
	return thread.id
}

//获得线程名称
func (thread *Thread) GetName() string {
	return thread.name
}

//获得线程状态
func (thread *Thread) GetStatus() int {
	return thread.status
}

//获得线程回调函数名称
func (thread *Thread) GetCallbackName() string {
	return thread.callbackName
}

//获得线程的起始执行时间
func (thread *Thread) GetStartTime() int {
	return thread.startTime
}

//获得线程的已执行时间
func (thread *Thread) GetExecTime() int {
	return 0
}
