package ClientDefault

import (
	"../../../../System/Application/Run"
	"fmt"
)

type Site interface {
	Index()
}

type ClientSite struct {
}

func (clientSite ClientSite) Index(params Run.Params) {
	fmt.Println("调用控制器方法")
	fmt.Println(params)
}
