package main

import (
	"./System/Application/Const"
	"./System/Application/Controller/SystemDefault"
	"./System/Application/Run"
	"flag"
	"fmt"
	"strings"
)

func main() {
	if Const.RUN_MODE == Const.RUN_MODE_CLI {
		cli()
	} else if Const.RUN_MODE == Const.RUN_MODE_WEB {
		web()
	} else if Const.RUN_MODE == Const.RUN_MODE_SOCKET_TCP {
		socket_tcp()
	} else if Const.RUN_MODE == Const.RUN_MODE_SOCKET_UDP {
		socket_udp()
	}
}

func cli() {
	//获取命令行参数
	url := flag.String("url", "server/default/site/index", "default url")
	post := flag.String("post", "test=1,debug=1", "default post")
	flag.Parse()
	//
	fmt.Println("路由路径 : " + (*url))
	fmt.Println("路由参数 : " + (*post))
	//
	var params Run.Params
	params.Url = *url                      //url路径
	params.Datas = make(map[string]string) //url参数
	//
	postInfo := strings.Split(*post, ",")
	for index, item := range postInfo {
		_ = index //声明该变量将不被使用
		postItemInfo := strings.Split(item, "=")
		params.Datas[postItemInfo[0]] = postItemInfo[1]
	}
	//调用框架的启动方法
	(new(SystemDefault.SystemSite)).Main(params)
}

func web() {
	(new(Run.Web)).Listen(Const.SERVER_PORT)
}

func socket_tcp() {

}

func socket_udp() {

}
